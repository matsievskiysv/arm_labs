#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define RCC 0x4002##1000##UL

#define RCC_CR	     0x00
#define RCC_CFGR     0x04
#define RCC_CIR	     0x08
#define RCC_APB2RSTR 0x0C
#define RCC_APB1RSTR 0x10
#define RCC_AHBENR   0x14
#define RCC_APB2ENR  0x18
#define RCC_APB1ENR  0x1C
#define RCC_BDCR     0x20
#define RCC_CSR	     0x24
#define RCC_AHBRSTR  0x28
#define RCC_CFGR2    0x2C

typedef union {
	struct {
		bool hsion	: 1;
		bool hsirdy	: 1;
		bool		: 1;
		uint8_t hsitrim : 5;
		uint8_t hsical	: 8;
		bool	hseon	: 1;
		bool	hserdy	: 1;
		bool	hsebyp	: 1;
		bool	csson	: 1;
		uint8_t		: 4;
		bool pllon	: 1;
		bool pllrdy	: 1;
	} bits;
	size_t val;
} rcc_cr_t;

typedef union {
	struct {
		uint8_t sw	 : 2;
		uint8_t sws	 : 2;
		uint8_t hpre	 : 4;
		uint8_t ppre1	 : 3;
		uint8_t ppre2	 : 3;
		uint8_t adcpre	 : 2;
		bool	pplsrc	 : 1;
		bool	pplxtpre : 1;
		uint8_t pplmul	 : 4;
		bool	usbpre	 : 1;
		bool		 : 1;
		uint8_t mco	 : 3;
	} bits;
	size_t val;
} rcc_cfgr_t;

#define RCC_SW_HSI 0b00
#define RCC_SW_HSE 0b01
#define RCC_SW_PLL 0b10

#define RCC_SWS_HSI RCC_SW_HSI
#define RCC_SWS_HSE RCC_SW_HSE
#define RCC_SWS_PLL RCC_SW_PLL

#define RCC_HPRE_1   0b0000
#define RCC_HPRE_2   0b1000
#define RCC_HPRE_4   0b1001
#define RCC_HPRE_8   0b1010
#define RCC_HPRE_16  0b1011
#define RCC_HPRE_64  0b1100
#define RCC_HPRE_128 0b1101
#define RCC_HPRE_256 0b1110
#define RCC_HPRE_512 0b1111

#define RCC_PPRE1_1  0b000
#define RCC_PPRE1_2  0b100
#define RCC_PPRE1_4  0b101
#define RCC_PPRE1_8  0b110
#define RCC_PPRE1_16 0b111

#define RCC_PPRE2_1  0b000
#define RCC_PPRE2_2  0b100
#define RCC_PPRE2_4  0b101
#define RCC_PPRE2_8  0b110
#define RCC_PPRE2_16 0b111

#define RCC_ADCPRE_1 0b00
#define RCC_ADCPRE_2 0b01
#define RCC_ADCPRE_4 0b10
#define RCC_ADCPRE_8 0b11

#define RCC_PLLSRC_HSI 0
#define RCC_PLLSRC_HSE 1

#define RCC_PPLMUL_2X  0b0000
#define RCC_PPLMUL_3X  0b0001
#define RCC_PPLMUL_4X  0b0010
#define RCC_PPLMUL_5X  0b0011
#define RCC_PPLMUL_6X  0b0100
#define RCC_PPLMUL_7X  0b0101
#define RCC_PPLMUL_8X  0b0110
#define RCC_PPLMUL_9X  0b0111
#define RCC_PPLMUL_10X 0b1000
#define RCC_PPLMUL_11X 0b1001
#define RCC_PPLMUL_12X 0b1010
#define RCC_PPLMUL_13X 0b1011
#define RCC_PPLMUL_14X 0b1100
#define RCC_PPLMUL_15X 0b1101
#define RCC_PPLMUL_16X 0b1110

#define RCC_MCO_OFF    0b000
#define RCC_MCO_SYSCLK 0b100
#define RCC_MCO_HSI    0b101
#define RCC_MCO_HSE    0b110
#define RCC_MCO_PLL    0b111

typedef union {
	struct {
		bool lsirdyf  : 1;
		bool lserdyf  : 1;
		bool hsirdyf  : 1;
		bool hserdyf  : 1;
		bool lpprdyf  : 1;
		uint8_t	      : 2;
		bool cssf     : 1;
		bool lsirdyie : 1;
		bool lserdyie : 1;
		bool hsirdyie : 1;
		bool hserdyie : 1;
		bool lpprdyie : 1;
		uint8_t	      : 3;
		bool lsirdyc  : 1;
		bool lserdyc  : 1;
		bool hsirdyc  : 1;
		bool hserdyc  : 1;
		bool lpprdyc  : 1;
		uint8_t	      : 2;
		bool cssc     : 1;
	} bits;
	size_t val;
} rcc_cir_t;

typedef union {
	struct {
		bool afiorst   : 1;
		bool	       : 1;
		bool ioparst   : 1;
		bool iopbrst   : 1;
		bool iopcrst   : 1;
		bool iopdrst   : 1;
		bool ioperst   : 1;
		bool iopfrst   : 1;
		bool iopgrst   : 1;
		bool adc1rst   : 1;
		bool adc2rst   : 1;
		bool tim1rst   : 1;
		bool spi1rst   : 1;
		bool tim8rst   : 1;
		bool usart1rst : 1;
		bool adc3rst   : 1;
		uint8_t	       : 3;
		bool tim9rst   : 1;
		bool tim10rst  : 1;
		bool tim11rst  : 1;
	} bits;
	size_t val;
} rcc_apb2rstr_t;

typedef union {
	struct {
		bool tim2rst   : 1;
		bool tim3rst   : 1;
		bool tim4rst   : 1;
		bool tim5rst   : 1;
		bool tim6rst   : 1;
		bool tim7rst   : 1;
		bool tim12rst  : 1;
		bool tim13rst  : 1;
		bool tim14rst  : 1;
		uint8_t	       : 2;
		bool wwdgrst   : 1;
		uint8_t	       : 2;
		bool spi2rst   : 1;
		bool spi3rst   : 1;
		bool	       : 1;
		bool usart2rst : 1;
		bool usart3rst : 1;
		bool uart4rst  : 1;
		bool uart5rst  : 1;
		bool i2c1rst   : 1;
		bool i2c2rst   : 1;
		bool usbrst    : 1;
		bool	       : 1;
		bool canrst    : 1;
		bool	       : 1;
		bool bkprst    : 1;
		bool pwrrst    : 1;
		bool dacrst    : 1;
	} bits;
	size_t val;
} rcc_apb1rstr_t;

typedef union {
	struct {
		bool dma1en  : 1;
		bool dma2en  : 1;
		bool sramen  : 1;
		bool	     : 1;
		bool flitfen : 1;
		bool	     : 1;
		bool crcen   : 1;
		bool	     : 1;
		bool fsmcen  : 1;
		bool	     : 1;
		bool sdioen  : 1;
	} bits;
	size_t val;
} rcc_ahbenr_t;

typedef union {
	struct {
		bool afioen   : 1;
		bool	      : 1;
		bool iopaen   : 1;
		bool iopben   : 1;
		bool iopcen   : 1;
		bool iopden   : 1;
		bool iopeen   : 1;
		bool iopfen   : 1;
		bool iopgen   : 1;
		bool adc1en   : 1;
		bool adc2en   : 1;
		bool tim1en   : 1;
		bool spi1en   : 1;
		bool tim8en   : 1;
		bool usart1en : 1;
		bool adc3en   : 1;
		uint8_t	      : 3;
		bool tim9en   : 1;
		bool tim10en  : 1;
		bool tim11en  : 1;
	} bits;
	size_t val;
} rcc_apb2enr_t;

typedef union {
	struct {
		bool tim2en   : 1;
		bool tim3en   : 1;
		bool tim4en   : 1;
		bool tim5en   : 1;
		bool tim6en   : 1;
		bool tim7en   : 1;
		bool tim12en  : 1;
		bool tim13en  : 1;
		bool tim14en  : 1;
		uint8_t	      : 2;
		bool wwdgen   : 1;
		uint8_t	      : 2;
		bool spi2en   : 1;
		bool spi3en   : 1;
		bool	      : 1;
		bool usart2en : 1;
		bool usart3en : 1;
		bool uart4en  : 1;
		bool uart5en  : 1;
		bool i2c1en   : 1;
		bool i2c2en   : 1;
		bool usben    : 1;
		bool	      : 1;
		bool canen    : 1;
		bool	      : 1;
		bool bkpen    : 1;
		bool pwren    : 1;
		bool dacen    : 1;
	} bits;
	size_t val;
} rcc_apb1enr_t;

typedef union {
	struct {
		bool lseon     : 1;
		bool lserdy    : 1;
		bool lsebyp    : 1;
		uint8_t	       : 5;
		uint8_t rtcsel : 2;
		uint8_t	       : 5;
		bool rtcen     : 1;
		bool bdrst     : 1;
	} bits;
	size_t val;
} rcc_bdcr_t;

typedef union {
	struct {
		bool lsion    : 1;
		bool lsirdy   : 1;
		size_t	      : 21;
		bool rmvf     : 1;
		bool	      : 1;
		bool pinrstf  : 1;
		bool porrstf  : 1;
		bool sftrstf  : 1;
		bool iwdgrstf : 1;
		bool wwdgrstf : 1;
		bool lpwrrstf : 1;
	} bits;
	size_t val;
} rcc_csr_t;

typedef union {
	struct {
		uint16_t       : 12;
		bool orgfsrst  : 1;
		bool	       : 1;
		bool ethmacrst : 1;
	} bits;
	size_t val;
} rcc_ahbrsrt_t;

typedef union {
	struct {
		uint8_t prediv1	 : 4;
		bool	orgfsrst : 1;
		bool		 : 1;
		bool ethmacrst	 : 1;
	} bits;
	size_t val;
} rcc_cfgr2_t;

#define RCC_PREDIV1_1  0b0000
#define RCC_PREDIV1_2  0b0001
#define RCC_PREDIV1_3  0b0010
#define RCC_PREDIV1_4  0b0011
#define RCC_PREDIV1_5  0b0100
#define RCC_PREDIV1_6  0b0101
#define RCC_PREDIV1_7  0b0110
#define RCC_PREDIV1_8  0b0111
#define RCC_PREDIV1_9  0b1000
#define RCC_PREDIV1_10 0b1001
#define RCC_PREDIV1_11 0b1010
#define RCC_PREDIV1_12 0b1011
#define RCC_PREDIV1_13 0b1100
#define RCC_PREDIV1_14 0b1101
#define RCC_PREDIV1_15 0b1110
#define RCC_PREDIV1_16 0b1111
