#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define GPIO_A 0x4001##0800##UL
#define GPIO_B 0x4001##0C00##UL
#define GPIO_C 0x4001##1000##UL
#define GPIO_D 0x4001##1400##UL
#define GPIO_E 0x4001##1800##UL
#define GPIO_F 0x4001##1C00##UL
#define GPIO_G 0x4001##2000##UL

#define GPIO_CRL  0x00
#define GPIO_CRH  0x04
#define GPIO_IDR  0x08
#define GPIO_ODR  0x0C
#define GPIO_BSSR 0x10
#define GPIO_BRR  0x14
#define GPIO_LCKR 0x18

typedef union {
	struct {
		uint8_t mode1 : 2;
		uint8_t cnf1  : 2;
		uint8_t mode2 : 2;
		uint8_t cnf2  : 2;
		uint8_t mode3 : 2;
		uint8_t cnf3  : 2;
		uint8_t mode4 : 2;
		uint8_t cnf4  : 2;
		uint8_t mode5 : 2;
		uint8_t cnf5  : 2;
		uint8_t mode6 : 2;
		uint8_t cnf6  : 2;
		uint8_t mode7 : 2;
		uint8_t cnf7  : 2;
		uint8_t mode8 : 2;
		uint8_t cnf8  : 2;
	} bits;
	size_t val;
} gpio_crl_t;

typedef union {
	struct {
		uint8_t mode9 : 2;
		uint8_t cnf9  : 2;
		uint8_t mode10 : 2;
		uint8_t cnf10  : 2;
		uint8_t mode11 : 2;
		uint8_t cnf11  : 2;
		uint8_t mode12 : 2;
		uint8_t cnf12  : 2;
		uint8_t mode13 : 2;
		uint8_t cnf13  : 2;
		uint8_t mode14 : 2;
		uint8_t cnf14  : 2;
		uint8_t mode15 : 2;
		uint8_t cnf15  : 2;
		uint8_t mode16 : 2;
		uint8_t cnf16  : 2;
	} bits;
	size_t val;
} gpio_crh_t;

#define GPIO_IN_ANALOG 0b00
#define GPIO_IN_FLOAT  0b01
#define GPIO_IN_PULL   0b10
#define GPIO_OUT_PUSH  0b00
#define GPIO_OUT_OPEN  0b01
#define GPIO_ALT_PUSH  0b10
#define GPIO_ALT_OPEN  0b11

#define GPIO_SPEED_2MHZ	 0b10
#define GPIO_SPEED_10MHZ 0b01
#define GPIO_SPEED_50MHZ 0b11

typedef union {
	struct {
		uint16_t idr : 16;
	} bits;
	size_t val;
} gpio_idr_t;

typedef union {
	struct {
		uint16_t odr : 16;
	} bits;
	size_t val;
} gpio_odr_t;

typedef union {
	struct {
		uint16_t br : 16;
		uint16_t bs : 16;
	} bits;
	size_t val;
} gpio_bsrr_t;

typedef union {
	struct {
		uint16_t br : 16;
		uint16_t bs : 16;
	} bits;
	size_t val;
} gpio_brr_t;

typedef union {
	struct {
		uint16_t lck  : 16;
		bool	 lckk : 1;
	} bits;
	size_t val;
} gpio_lckr_t;
