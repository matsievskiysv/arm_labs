#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define DMA1 0x4002##0000##UL
#define DMA2 0x4002##4000##UL

#define DMA_ISR	 0x00
#define DMA_IFCR 0x04

#define DMA_CH(n) (20 * (n - 1))

#define DMAx_CCRx   0x08
#define DMAx_CNDTRx 0x0C
#define DMAx_CPARx  0x10
#define DMAx_CMARx  0x14

typedef union {
	struct {
		bool gif1  : 1;
		bool tcif1 : 1;
		bool htif1 : 1;
		bool teif1 : 1;
		bool gif2  : 1;
		bool tcif2 : 1;
		bool htif2 : 1;
		bool teif2 : 1;
		bool gif3  : 1;
		bool tcif3 : 1;
		bool htif3 : 1;
		bool teif3 : 1;
		bool gif4  : 1;
		bool tcif4 : 1;
		bool htif4 : 1;
		bool teif4 : 1;
		bool gif5  : 1;
		bool tcif5 : 1;
		bool htif5 : 1;
		bool teif5 : 1;
		bool gif6  : 1;
		bool tcif6 : 1;
		bool htif6 : 1;
		bool teif6 : 1;
		bool gif7  : 1;
		bool tcif7 : 1;
		bool htif7 : 1;
		bool teif7 : 1;
	} bits;
	size_t val;
} dma_isr_t;

typedef union {
	struct {
		bool cgif1  : 1;
		bool ctcif1 : 1;
		bool chtif1 : 1;
		bool cteif1 : 1;
		bool cgif2  : 1;
		bool ctcif2 : 1;
		bool chtif2 : 1;
		bool cteif2 : 1;
		bool cgif3  : 1;
		bool ctcif3 : 1;
		bool chtif3 : 1;
		bool cteif3 : 1;
		bool cgif4  : 1;
		bool ctcif4 : 1;
		bool chtif4 : 1;
		bool cteif4 : 1;
		bool cgif5  : 1;
		bool ctcif5 : 1;
		bool chtif5 : 1;
		bool cteif5 : 1;
		bool cgif6  : 1;
		bool ctcif6 : 1;
		bool chtif6 : 1;
		bool cteif6 : 1;
		bool cgif7  : 1;
		bool ctcif7 : 1;
		bool chtif7 : 1;
		bool cteif7 : 1;
	} bits;
	size_t val;
} dma_ifcr_t;

typedef union {
	struct {
		bool	en	: 1;
		bool	tcie	: 1;
		bool	htie	: 1;
		bool	teie	: 1;
		bool	dir	: 1;
		bool	circ	: 1;
		bool	pinc	: 1;
		bool	minc	: 1;
		uint8_t psize	: 2;
		uint8_t msize	: 2;
		uint8_t pl	: 2;
		bool	mem2mem : 1;
	} bits;
	size_t val;
} dma_ccr_t;

#define DMA_PL_LO  0b00
#define DMA_PL_MED 0b01
#define DMA_PL_HI  0b10
#define DMA_PL_VHI 0b11

#define DMA_SIZE_8BIT  0b00
#define DMA_SIZE_16BIT 0b01
#define DMA_SIZE_32BIT 0b10

typedef union {
	struct {
		uint16_t ndt : 16;
	} bits;
	size_t val;
} dma_cndtr_t;

typedef union {
	struct {
		size_t pa : 32;
	} bits;
	size_t val;
} dma_cpar_t;

typedef union {
	struct {
		size_t ma : 32;
	} bits;
	size_t val;
} dma_cmar_t;

#define DMA_CH_ADC1	 1
#define DMA_CH_TIM2_CH3	 1
#define DMA_CH_TIM4_CH1	 1
#define DMA_CH_USART3_TX 2
#define DMA_CH_TIM1_CH1	 2
#define DMA_CH_TIM2_UP	 2
#define DMA_CH_TIM3_CH3	 2
#define DMA_CH_SPI1_RX	 2
#define DMA_CH_USART3_RX 3
#define DMA_CH_TIM1_CH2	 3
#define DMA_CH_TIM3_CH4	 3
#define DMA_CH_TIM3_UP	 3
#define DMA_CH_SPI1_TX	 3
#define DMA_CH_USART1_TX 4
#define DMA_CH_TIM1_CH4	 4
#define DMA_CH_TIM1_TRIG 4
#define DMA_CH_TIM1_COM	 4
#define DMA_CH_TIM4_CH2	 4
#define DMA_CH_SPI2_RX	 4
#define DMA_CH_I2S2_RX	 4
#define DMA_CH_I2C2_TX	 4
#define DMA_CH_USART1_RX 5
#define DMA_CH_TIM1_UP	 5
#define DMA_CH_SPI2_TX	 5
#define DMA_CH_I2S2_TX	 5
#define DMA_CH_TIM2_CH1	 5
#define DMA_CH_TIM4_CH3	 5
#define DMA_CH_I2C2_RX	 5
#define DMA_CH_USART2_RX 6
#define DMA_CH_TIM1_CH3	 6
#define DMA_CH_TIM3_CH1	 6
#define DMA_CH_TIM3_TRIG 6
#define DMA_CH_I2C1_TX	 6
#define DMA_CH_USART2_TX 7
#define DMA_CH_TIM2_CH2	 7
#define DMA_CH_TIM2_CH4	 7
#define DMA_CH_TIM4_UP	 7
#define DMA_CH_I2C1_RX	 7
