#pragma once

#define EXTI 0x4001##0400##UL

#define EXTI_IMR   0x00
#define EXTI_EMR   0x04
#define EXTI_RTSR  0x08
#define EXTI_FTSR  0x0C
#define EXTI_SWIER 0x10
#define EXTI_PR	   0x14
