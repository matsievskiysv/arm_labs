#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define ADC1 0x4001##2400##UL
#define ADC2 0x4001##2800##UL
#define ADC3 0x4001##3C00##UL

#define ADC_SR	  0x00
#define ADC_CR1	  0x04
#define ADC_CR2	  0x08
#define ADC_SMPR1 0x0C
#define ADC_SMPR2 0x10
#define ADC_JOFR1 0x14
#define ADC_JOFR2 0x18
#define ADC_JOFR3 0x1C
#define ADC_JOFR4 0x20
#define ADC_HTR	  0x24
#define ADC_LTR	  0x28
#define ADC_SQR1  0x2C
#define ADC_SQR2  0x30
#define ADC_SQR3  0x34
#define ADC_JSQR  0x38
#define ADC_JDR1  0x3C
#define ADC_JDR2  0x40
#define ADC_JDR3  0x44
#define ADC_JDR4  0x48
#define ADC_DR	  0x4C

typedef union {
	struct {
		bool awd   : 1;
		bool eoc   : 1;
		bool jeoc  : 1;
		bool jstrt : 1;
		bool strt  : 1;
	} bits;
	size_t val;
} adc_sr_t;

typedef union {
	struct {
		uint8_t awdch	: 5;
		bool	eocie	: 1;
		bool	awdie	: 1;
		bool	jeocie	: 1;
		bool	scan	: 1;
		bool	awdsgl	: 1;
		bool	jauto	: 1;
		bool	discen	: 1;
		bool	jdiscen : 1;
		uint8_t discnum : 3;
		uint8_t dualmod : 3;
		uint8_t		: 2;
		bool jawden	: 1;
		bool awden	: 1;
	} bits;
	size_t val;
} adc_cr1_t;

#define ADC_DUALMOD_IND		     0b0000
#define ADC_DUALMOD_COM_REG_INJ_SIM  0b0001
#define ADC_DUALMOD_COM_REG_ALT_TR   0b0010
#define ADC_DUALMOD_COM_INJ_FAST_INT 0b0011
#define ADC_DUALMOD_COM_INJ_SLOW_INT 0b0100
#define ADC_DUALMOD_INJ_SIM	     0b0101
#define ADC_DUALMOD_REG_SIM	     0b0110
#define ADC_DUALMOD_FAST_INT	     0b0111
#define ADC_DUALMOD_SLOW_INT	     0b1000
#define ADC_DUALMOD_ALT		     0b1001

#define ADC_DISCNUM_1 0b000
#define ADC_DISCNUM_2 0b001
#define ADC_DISCNUM_3 0b010
#define ADC_DISCNUM_4 0b011
#define ADC_DISCNUM_5 0b100
#define ADC_DISCNUM_6 0b101
#define ADC_DISCNUM_7 0b110
#define ADC_DISCNUM_8 0b111

#define ADC_AWDCH_0  0b00000
#define ADC_AWDCH_1  0b00001
#define ADC_AWDCH_2  0b00010
#define ADC_AWDCH_3  0b00011
#define ADC_AWDCH_4  0b00100
#define ADC_AWDCH_5  0b00101
#define ADC_AWDCH_6  0b00110
#define ADC_AWDCH_7  0b00111
#define ADC_AWDCH_8  0b01000
#define ADC_AWDCH_9  0b01001
#define ADC_AWDCH_10 0b01010
#define ADC_AWDCH_11 0b01011
#define ADC_AWDCH_12 0b01100
#define ADC_AWDCH_13 0b01101
#define ADC_AWDCH_14 0b01110
#define ADC_AWDCH_15 0b01111
#define ADC_AWDCH_16 0b10000
#define ADC_AWDCH_17 0b10001

typedef union {
	struct {
		bool adon	 : 1;
		bool cont	 : 1;
		bool cal	 : 1;
		bool rstcal	 : 1;
		uint8_t		 : 4;
		bool dma	 : 1;
		uint8_t		 : 2;
		bool	align	 : 1;
		uint8_t jextsel	 : 3;
		bool	jexttrig : 1;
		bool		 : 1;
		uint8_t extsel	 : 3;
		bool	extrtig	 : 1;
		bool	jswstart : 1;
		bool	swstart	 : 1;
		bool	tsvrefe	 : 1;
	} bits;
	size_t val;
} adc_cr2_t;

#define ADC1_EXTSEL_TIM_1_CC_1 0b000
#define ADC1_EXTSEL_TIM_1_CC_2 0b001
#define ADC1_EXTSEL_TIM_1_CC_3 0b010
#define ADC1_EXTSEL_TIM_2_CC_2 0b011
#define ADC1_EXTSEL_TIM_3_TRGO 0b100
#define ADC1_EXTSEL_TIM_4_CC_4 0b101
#define ADC1_EXTSEL_TIM_8_TRGO 0b110
#define ADC1_EXTSEL_EXTI_11    0b110
#define ADC1_EXTSEL_SWSTART    0b111

#define ADC2_EXTSEL_TIM_1_CC_1 ADC1_EXTSEL_TIM_1_CC_1
#define ADC2_EXTSEL_TIM_1_CC_2 ADC1_EXTSEL_TIM_1_CC_2
#define ADC2_EXTSEL_TIM_1_CC_3 ADC1_EXTSEL_TIM_1_CC_3
#define ADC2_EXTSEL_TIM_2_CC_2 ADC1_EXTSEL_TIM_2_CC_2
#define ADC2_EXTSEL_TIM_3_TRGO ADC1_EXTSEL_TIM_3_TRGO
#define ADC2_EXTSEL_TIM_4_CC_4 ADC1_EXTSEL_TIM_4_CC_4
#define ADC2_EXTSEL_TIM_8_TRGO ADC1_EXTSEL_TIM_8_TRGO
#define ADC2_EXTSEL_EXTI_11    ADC1_EXTSEL_EXTI_11
#define ADC2_EXTSEL_SWSTART    ADC1_EXTSEL_SWSTART

#define ADC3_EXTSEL_TIM_3_CC_1 0b000
#define ADC3_EXTSEL_TIM_2_CC_3 0b001
#define ADC3_EXTSEL_TIM_1_CC_3 0b010
#define ADC3_EXTSEL_TIM_8_CC_1 0b011
#define ADC3_EXTSEL_TIM_8_TRGO 0b100
#define ADC3_EXTSEL_TIM_5_CC_1 0b101
#define ADC3_EXTSEL_TIM_5_CC_3 0b110
#define ADC3_EXTSEL_SWSTART    0b111

#define ADC1_JEXTSEL_TIM_1_TRGO 0b000
#define ADC1_JEXTSEL_TIM_1_CC_4 0b001
#define ADC1_JEXTSEL_TIM_2_TRGO 0b010
#define ADC1_JEXTSEL_TIM_2_CC_1 0b011
#define ADC1_JEXTSEL_TIM_3_CC_4 0b100
#define ADC1_JEXTSEL_TIM_4_TRGO 0b101
#define ADC1_JEXTSEL_TIM_8_CC_4 0b110
#define ADC1_JEXTSEL_EXTI_15	0b110
#define ADC1_JEXTSEL_JWSTART	0b111

#define ADC2_JEXTSEL_TIM_1_TRGO ADC1_JEXTSEL_TIM_1_TRGO
#define ADC2_JEXTSEL_TIM_1_CC_4 ADC1_JEXTSEL_TIM_1_CC_4
#define ADC2_JEXTSEL_TIM_2_TRGO ADC1_JEXTSEL_TIM_2_TRGO
#define ADC2_JEXTSEL_TIM_2_CC_1 ADC1_JEXTSEL_TIM_2_CC_1
#define ADC2_JEXTSEL_TIM_3_CC_4 ADC1_JEXTSEL_TIM_3_CC_4
#define ADC2_JEXTSEL_TIM_4_TRGO ADC1_JEXTSEL_TIM_4_TRGO
#define ADC2_JEXTSEL_TIM_8_CC_4 ADC1_JEXTSEL_TIM_8_CC_4
#define ADC2_JEXTSEL_EXTI_15	ADC1_JEXTSEL_EXTI_15
#define ADC2_JEXTSEL_JWSTART	ADC1_JEXTSEL_JWSTART

#define ADC3_JEXTSEL_TIM_1_TRGO 0b000
#define ADC3_JEXTSEL_TIM_1_CC_4 0b001
#define ADC3_JEXTSEL_TIM_4_CC_3 0b010
#define ADC3_JEXTSEL_TIM_8_CC_2 0b011
#define ADC3_JEXTSEL_TIM_8_CC_4 0b100
#define ADC3_JEXTSEL_TIM_5_TRGO 0b101
#define ADC3_JEXTSEL_TIM_5_CC_4 0b110
#define ADC3_JEXTSEL_JWSTART	0b111

#define ADC_ALIGN_RIGHT 0
#define ADC_ALIGN_LEFT	1

typedef union {
	struct {
		uint8_t smp10 : 3;
		uint8_t smp11 : 3;
		uint8_t smp12 : 3;
		uint8_t smp13 : 3;
		uint8_t smp14 : 3;
		uint8_t smp15 : 3;
		uint8_t smp16 : 3;
		uint8_t smp17 : 3;
	} bits;
	size_t val;
} adc_smpr1_t;

#define ADC_SMP_1_5   0b000
#define ADC_SMP_7_5   0b001
#define ADC_SMP_13_5  0b010
#define ADC_SMP_28_5  0b011
#define ADC_SMP_41_5  0b100
#define ADC_SMP_55_5  0b101
#define ADC_SMP_71_5  0b110
#define ADC_SMP_239_5 0b111

typedef union {
	struct {
		uint8_t smp0 : 3;
		uint8_t smp1 : 3;
		uint8_t smp2 : 3;
		uint8_t smp3 : 3;
		uint8_t smp4 : 3;
		uint8_t smp5 : 3;
		uint8_t smp6 : 3;
		uint8_t smp7 : 3;
		uint8_t smp8 : 3;
		uint8_t smp9 : 3;
	} bits;
	size_t val;
} adc_smpr2_t;

typedef union {
	struct {
		uint16_t joffset : 12;
	} bits;
	size_t val;
} adc_jofr_t;

typedef union {
	struct {
		uint16_t ht : 12;
	} bits;
	size_t val;
} adc_htr_t;

typedef union {
	struct {
		uint16_t lt : 12;
	} bits;
	size_t val;
} adc_ltr_t;

typedef union {
	struct {
		uint8_t sq13 : 5;
		uint8_t sq14 : 5;
		uint8_t sq15 : 5;
		uint8_t sq16 : 5;
		uint8_t l    : 4;
	} bits;
	size_t val;
} adc_sqr1_t;

#define ADC_L_1	 0b0000
#define ADC_L_2	 0b0001
#define ADC_L_3	 0b0010
#define ADC_L_4	 0b0011
#define ADC_L_5	 0b0100
#define ADC_L_6	 0b0101
#define ADC_L_7	 0b0110
#define ADC_L_8	 0b0111
#define ADC_L_9	 0b1000
#define ADC_L_10 0b1001
#define ADC_L_11 0b1010
#define ADC_L_12 0b1011
#define ADC_L_13 0b1100
#define ADC_L_14 0b1101
#define ADC_L_15 0b1110
#define ADC_L_16 0b1111

typedef union {
	struct {
		uint8_t sq7  : 5;
		uint8_t sq8  : 5;
		uint8_t sq9  : 5;
		uint8_t sq10 : 5;
		uint8_t sq11 : 5;
		uint8_t sq12 : 5;
	} bits;
	size_t val;
} adc_sqr2_t;

typedef union {
	struct {
		uint8_t sq1 : 5;
		uint8_t sq2 : 5;
		uint8_t sq3 : 5;
		uint8_t sq4 : 5;
		uint8_t sq5 : 5;
		uint8_t sq6 : 5;
	} bits;
	size_t val;
} adc_sqr3_t;

typedef union {
	struct {
		uint8_t jsq1 : 5;
		uint8_t jsq2 : 5;
		uint8_t jsq3 : 5;
		uint8_t jsq4 : 5;
		uint8_t jl   : 2;
	} bits;
	size_t val;
} adc_jsqr_t;

#define ADC_JL_1 0b00
#define ADC_JL_2 0b01
#define ADC_JL_3 0b10
#define ADC_JL_4 0b11

typedef union {
	struct {
		uint16_t jdata : 16;
	} bits;
	size_t val;
} adc_jdr_t;

typedef union {
	struct {
		uint16_t adc2data : 16;
		uint16_t data	  : 16;
	} bits;
	size_t val;
} adc_dr_t;
