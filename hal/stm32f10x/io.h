#pragma once

#define FLASH	      0x0800##0000##UL
#define RAM	      0x2000##0000##UL
#define SYSTEM_MEMORY 0x1FFF##F000##UL
#define PERIPHERALS   0x4000##0000##UL
