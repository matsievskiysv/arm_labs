#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define FLASH_SZ  0x1FFF##F7E0##UL
#define UNIQUE_ID 0x1FFF##F7E8##UL

typedef union {
	struct {
		uint16_t f_size : 16;
	} bits;
	size_t val;
} f_size;
