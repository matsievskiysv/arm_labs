#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define SYST_CSR   0xE000##E010##UL
#define SYST_RVR   0xE000##E014##UL
#define SYST_CVR   0xE000##E018##UL
#define SYST_CALIB 0xE000##E01C##UL

typedef union {
	struct {
		bool enable    : 1;
		bool tickint   : 1;
		bool clksource : 1;
		uint16_t       : 12;
		bool countflag : 1;
	} bits;
	size_t val;
} syst_csr_t;

typedef union {
	struct {
		size_t reload : 24;
	} bits;
	size_t val;
} syst_rvr_t;

typedef union {
	struct {
		size_t current : 24;
	} bits;
	size_t val;
} syst_cvr_t;

typedef union {
	struct {
		size_t tenms : 24;
		uint8_t	     : 6;
		bool skew    : 1;
		bool noref   : 1;
	} bits;
	size_t val;
} syst_calib_t;
