#include <systick.h>
#include <util.h>

void
systick_configure(size_t value, bool interrupt)
{
	REG_T	   sys_ctl = GET_REG(SYST_CSR, 0);
	syst_csr_t csr	   = {.bits = {.tickint = interrupt, .clksource = true}};
	REG_SET(sys_ctl, csr.val);
	REG_T	   sys_rvr = GET_REG(SYST_RVR, 0);
	syst_rvr_t rvr	   = {.bits = {.reload = value}};
	REG_WRITE(sys_rvr, rvr.val);
}

void
systick_start(void)
{
	REG_T	   sys_ctl = GET_REG(SYST_CSR, 0);
	syst_csr_t csr	   = {.bits = {.enable = true}};
	REG_SET(sys_ctl, csr.val);
}

void
systick_stop(void)
{
	REG_T	   sys_ctl = GET_REG(SYST_CSR, 0);
	syst_csr_t csr	   = {.bits = {.enable = true}};
	REG_CLEAR(sys_ctl, csr.val);
}

size_t
systick_current(void)
{
	REG_T	   sys_cvr = GET_REG(SYST_CVR, 0);
	syst_cvr_t cvr	   = {.val = REG_VAL(sys_cvr)};
	return cvr.bits.current;
}

bool
systick_expired(void)
{
	REG_T	   sys_ctl = GET_REG(SYST_CSR, 0);
	syst_csr_t csr	   = {.val = REG_VAL(sys_ctl)};
	return csr.bits.countflag;
}
