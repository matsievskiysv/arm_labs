#include <adc.h>
#include <util.h>

void
adc_temp_config(bool int_enable, bool dma_enable)
{
	REG_T	  cr1_reg = GET_REG(ADC1, ADC_CR1);
	adc_cr1_t cr1	  = {.bits = {.eocie = int_enable, .scan = true}};
	REG_SET(cr1_reg, cr1.val);
	REG_T cr2_reg = GET_REG(ADC1, ADC_CR2);
	// enable ADC
	adc_cr2_t cr2 = {.bits = {.tsvrefe = true, .dma = dma_enable, .adon = true}};
	REG_SET(cr2_reg, cr2.val);
	REG_T smpr2_reg = GET_REG(ADC1, ADC_SMPR2);
	// set maximum sample time for temp and ref channels
	adc_smpr2_t st2 = {.bits = {.smp6 = ADC_SMP_239_5, .smp7 = ADC_SMP_239_5}};
	REG_SET(smpr2_reg, st2.val);
	REG_T sqr1_reg = GET_REG(ADC1, ADC_SQR1);
	// set two channels
	adc_sqr1_t sqr1 = {.bits = {.l = ADC_L_2}};
	REG_SET(sqr1_reg, sqr1.val);
	REG_T sqr3_reg = GET_REG(ADC1, ADC_SQR3);
	// set temp and ref channels
	adc_sqr3_t sqr3 = {.bits = {.sq1 = 16, .sq2 = 17}};
	REG_SET(sqr3_reg, sqr3.val);
}

void
adc_calibrate(size_t adc)
{
	REG_T cr2_reg = GET_REG(adc, ADC_CR2);
	// reset
	{
		adc_cr2_t cr2 = {.bits = {.rstcal = true}};
		REG_SET(cr2_reg, cr2.val);
	}
	// calibrate
	adc_cr2_t cr2 = {.bits = {.cal = true}};
	REG_SET(cr2_reg, cr2.val);
	WAIT_WHILE(((adc_cr2_t) REG_VAL(cr2_reg)).bits.cal);
}

void
adc_temp_start(void)
{
	REG_T sr_reg = GET_REG(ADC1, ADC_SR);
	// clear status register
	adc_sr_t sr = {.bits = {.awd = true, .eoc = true, .jeoc = true, .jstrt = true, .strt = true}};
	REG_CLEAR(sr_reg, sr.val);
	REG_T cr2_reg = GET_REG(ADC1, ADC_CR2);
	adc_cr2_t cr2	  = {.bits = {.adon = true}};
	REG_SET(cr2_reg, cr2.val);
}

int32_t
adc_temp_convert(int16_t temp_code, int16_t ref_code)
{
#ifdef __FPU_PRESENT
	return (1.43 - (1.2 * temp_code) / ref_code) / 0.0043 * 1000 + 25000;
#else
	return (14300 - (12000L * temp_code) / ref_code) * 1000 / 43 + 25000;
#endif
}
