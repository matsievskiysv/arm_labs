#include <dma.h>
#include <util.h>

void
dma_config(size_t dma, uint8_t channel, void *mem, void *phr, uint16_t size, bool mem2mem, uint8_t level,
	   uint8_t mem_sz, uint8_t phr_sz, bool mem_inc, bool phr_inc, bool circ, bool mem2phr, bool err_int,
	   bool half_int, bool full_int)
{
	REG_T conf_reg = GET_REG(dma + DMA_CH(channel), DMAx_CCRx);
	REG_T num_reg  = GET_REG(dma + DMA_CH(channel), DMAx_CNDTRx);
	REG_T phr_reg  = GET_REG(dma + DMA_CH(channel), DMAx_CPARx);
	REG_T mem_reg  = GET_REG(dma + DMA_CH(channel), DMAx_CMARx);
	{
		dma_ccr_t crr = {.bits = {.en = true}};
		REG_CLEAR(conf_reg, crr.val);
	}
	dma_ccr_t crr = {.bits = {.mem2mem = mem2mem,
				  .pl	   = level,
				  .msize   = mem_sz,
				  .psize   = phr_sz,
				  .minc	   = mem_inc,
				  .pinc	   = phr_inc,
				  .circ	   = circ,
				  .dir	   = mem2phr,
				  .teie	   = err_int,
				  .htie	   = half_int,
				  .tcie	   = full_int}};
	REG_SET(conf_reg, crr.val);
	dma_cndtr_t cndtr = {.bits = {.ndt = size}};
	REG_SET(num_reg, cndtr.val);
	dma_cpar_t cpar = {.bits = {.pa = (size_t) phr}};
	REG_WRITE(phr_reg, cpar.val);
	dma_cmar_t cmar = {.bits = {.ma = (size_t) mem}};
	REG_WRITE(mem_reg, cmar.val);
}

dma_ccr_t
dma_get_config(size_t dma, uint8_t channel)
{
	REG_T conf_reg = GET_REG(dma + DMA_CH(channel), DMAx_CCRx);
	return (dma_ccr_t) REG_VAL(conf_reg);
}

size_t
dma_get_mem_address(size_t dma, uint8_t channel)
{
	REG_T mem_reg = GET_REG(dma + DMA_CH(channel), DMAx_CMARx);
	return REG_VAL(mem_reg);
}

size_t
dma_get_phy_address(size_t dma, uint8_t channel)
{
	REG_T phr_reg = GET_REG(dma + DMA_CH(channel), DMAx_CPARx);
	return REG_VAL(phr_reg);
}

uint16_t
dma_get_count(size_t dma, uint8_t channel)
{
	REG_T num_reg = GET_REG(dma + DMA_CH(channel), DMAx_CNDTRx);
	return ((dma_cndtr_t) REG_VAL(num_reg)).bits.ndt;
}

void
dma_set_count(size_t dma, uint8_t channel, uint16_t count)
{
	REG_T	    num_reg = GET_REG(dma + DMA_CH(channel), DMAx_CNDTRx);
	dma_cndtr_t cndtr   = {.bits = {.ndt = count}};
	REG_WRITE(num_reg, cndtr.val);
}

void
dma_set_memory_address(size_t dma, uint8_t channel, void *address)
{
	REG_T	   mem_reg = GET_REG(dma + DMA_CH(channel), DMAx_CMARx);
	dma_cmar_t cmar	   = {.bits = {.ma = (size_t) address}};
	REG_WRITE(mem_reg, cmar.val);
}

void
dma_set_peripheral_address(size_t dma, uint8_t channel, void *address)
{
	REG_T phr_reg = GET_REG(dma + DMA_CH(channel), DMAx_CPARx);

	dma_cpar_t cpar = {.bits = {.pa = (size_t) address}};
	REG_WRITE(phr_reg, cpar.val);
}

void
dma_enable(size_t dma, size_t channel)
{
	REG_T	  conf_reg = GET_REG(dma + DMA_CH(channel), DMAx_CCRx);
	dma_ccr_t crr	   = {.bits = {.en = true}};
	REG_SET(conf_reg, crr.val);
}

void
dma_disable(size_t dma, size_t channel)
{
	REG_T	  conf_reg = GET_REG(dma + DMA_CH(channel), DMAx_CCRx);
	dma_ccr_t crr	   = {.bits = {.en = true}};
	REG_CLEAR(conf_reg, crr.val);
}

dma_isr_t
dma_int_status(size_t dma)
{
	REG_T	  reg = GET_REG(dma, DMA_ISR);
	dma_isr_t isr = {.val = REG_VAL(reg)};
	return isr;
}

void
dma_int_clear(size_t dma, dma_ifcr_t mask)
{
	REG_T reg = GET_REG(dma, DMA_IFCR);
	REG_SET(reg, mask.val);
}
