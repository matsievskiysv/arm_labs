#include <rcc.h>
#include <util.h>

void
rcc_set_register(size_t reg, size_t value)
{
	REG_T rcc_reg = GET_REG(RCC, reg);
	REG_SET(rcc_reg, value);
}

void
rcc_write_register(size_t reg, size_t value)
{
	REG_T rcc_reg = GET_REG(RCC, reg);
	REG_WRITE(rcc_reg, value);
}

size_t
rcc_get_register(size_t reg)
{
	REG_T rcc_reg = GET_REG(RCC, reg);
	return REG_VAL(rcc_reg);
}

void
rcc_start_internal_clock(void)
{
	REG_T rcc_cr = GET_REG(RCC, RCC_CR);
	// enable 8MHz oscillator
	rcc_cr_t cr = {.bits = {.hsion = true}};
	REG_SET(rcc_cr, cr.val);
	// wait for 8MHz oscillator to stabilize
	cr.bits.hsion = false;
	cr.bits.hsirdy = true;
	WAIT_WHILE(!(REG_VAL(rcc_cr) & cr.val));
}

void rcc_adc_prescaler(size_t val) {
	rcc_cfgr_t cfgr = {.bits = {.adcpre = val}};
	rcc_set_register(RCC_CFGR, cfgr.val);
}
