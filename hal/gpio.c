#include <gpio.h>
#include <util.h>

void
gpio_configure_input(size_t bank, uint8_t pin_number, uint8_t mode, bool pull_level)
{
	REG_T reg = GET_REG(bank, pin_number >= 8 ? GPIO_CRH : GPIO_CRL);
	size_t val = mode << 2;
	size_t mask = 0xf;
	val <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	mask <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	// write all zeros before setting value
	REG_CLEAR(reg, mask);
	REG_SET(reg, val);
	if (mode == GPIO_IN_PULL) {
		REG_T odr = GET_REG(bank, GPIO_ODR);
		if (pull_level) {
			REG_SET(odr, 1 << pin_number);
		} else {
			REG_CLEAR(odr, 1 << pin_number);
		}
	}
}

void
gpio_configure_output(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t speed)
{
	REG_T reg = GET_REG(bank, pin_number >= 8 ? GPIO_CRH : GPIO_CRL);
	size_t val = mode << 2 | speed;
	size_t mask = 0xf;
	val <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	mask <<= (pin_number >= 8 ? pin_number - 8 : pin_number) * 4;
	// write all zeros before setting value
	REG_CLEAR(reg, mask);
	REG_SET(reg, val);
}

void
gpio_lock(size_t bank, size_t pin_mask)
{
	REG_T reg = GET_REG(bank, GPIO_LCKR);
	while (!(REG_VAL(reg) & BIT(16))) {
		// lock sequence
		REG_WRITE(reg, pin_mask | BIT(16));
		REG_WRITE(reg, pin_mask & ~BIT(16));
		REG_WRITE(reg, pin_mask | BIT(16));
		REG_VAL(reg);
	}
}

size_t
gpio_read_pins(size_t bank, size_t pin_mask)
{
	REG_T dr = GET_REG(bank, GPIO_IDR);
	return REG_VAL(dr) & pin_mask;
}

bool
gpio_read_pin(size_t bank, uint8_t pin_number)
{
	return !!gpio_read_pins(bank, BIT(pin_number));
}

void
gpio_write_pins(size_t bank, size_t pin_mask, bool value)
{
	REG_T reg = GET_REG(bank, value ? GPIO_BSSR : GPIO_BRR);
	if (value) {
		REG_SET(reg, pin_mask);
	} else {
		REG_CLEAR(reg, pin_mask);
	}
}

void
gpio_write_pin(size_t bank, uint8_t pin_number, bool value)
{
	gpio_write_pins(bank, BIT(pin_number), value);
}

void
gpio_toggle_pin(size_t bank, uint8_t pin_number)
{
	REG_T data = GET_REG(bank, GPIO_ODR);
	bool  val  = !!(REG_VAL(data) & BIT(pin_number));
	REG_T reg  = GET_REG(bank, val ? GPIO_BRR : GPIO_BSSR);
	REG_WRITE(reg, BIT(pin_number));
}
