#include <signature.h>
#include <util.h>

uint16_t
flash_size()
{
	REG_T reg = GET_REG(FLASH_SZ, 0);
	f_size sz  = {.val = REG_VAL(reg)};
	return sz.bits.f_size;
}

void
unique_id(unique_id_t id)
{
	id[0] = REG_VAL(GET_REG(UNIQUE_ID, sizeof(size_t) * 0));
	id[1] = REG_VAL(GET_REG(UNIQUE_ID, sizeof(size_t) * 1));
	id[2] = REG_VAL(GET_REG(UNIQUE_ID, sizeof(size_t) * 2));
}
