#pragma once

#include <stddef.h>

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/nvic.h"
#elif defined(set32f103h)
#include "../stm32f103h/nvic.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

/**
 * @brief Enable interrupt number \p N.
 *
 * @param[in] N Interrupt number.
 */
void enable_interrupt(size_t N);

/**
 * @brief Disable interrupt number \p N.
 *
 * @param[in] N Interrupt number.
 */
void disable_interrupt(size_t N);
