#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/usart.h"
#elif defined(set32f103h)
#include "../stm32f103h/usart.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

/**
 * @brief Configure USART port.
 *
 * @param[in] port USART port.
 * @param[in] nine_bit Eight or nine bit transmissions.
 * @param[in] parity Enable parity bit.
 * @param[in] even_parity Odd or even parity.
 * @param[in] send_break Send break character.
 * @param[in] stop_bits Number of stop bits. May be one of the USART_STOP_XX constants.
 * The 0.5 Stop bit and 1.5 Stop bit are not available for UART4 & UART5.
 */
void usart_configure(size_t port, bool nine_bit, bool parity, bool even_parity, bool send_break, size_t stop_bits);

/**
 * @brief USART port baudrate.
 * Baudrate is determined by the equation Tx/Rx baud = fCK/(16*USARTDIV),
 * where fCK is a peripheral clock value and
 * USARTDIV = ((mantissa & 0xfff0) << 4) | (fraction & 0xf).
 *
 * @param[in] port USART port.
 * @param[in] freq Clock frequency.
 * @param[in] baud Baud rate.
 */
void usart_baudrate(size_t port, int freq, int baud);

/**
 * @brief Query USART status.
 *
 * @param[in] port USART port.
 * @return USART status.
 */
usart_sr_t usart_status(size_t port);

/**
 * @brief Clear USART status bits.
 *
 * @param[in] port USART port.
 * @param[in] status USART status bit map.
 */
void usart_status_clear(size_t port, usart_sr_t status);

/**
 * @brief Read byte from USART.
 *
 * @param[in] port USART port.
 * @return USART data.
 */
uint8_t usart_read(size_t port);

/**
 * @brief Write byte to USART.
 *
 * @param[in] port USART port.
 * @param[in] data USART data.
 */
void usart_write(size_t port, uint8_t data);

/**
 * @brief Enable USART.
 *
 * @param[in] port USART port.
 */
void usart_enable(size_t port);

/**
 * @brief Disable USART.
 *
 * @param[in] port USART port.
 */
void usart_disable(size_t port);

/**
 * @brief Configure USART port interrupts.
 *
 * @param[in] port USART port.
 * @param[in] parity_err_int Parity error interrupt.
 * @param[in] tx_buf_empty_int Transmission buffer empty interrupt.
 * @param[in] tx_complete_int Transmission complete interrupt.
 * @param[in] rx_buf_not_empty_int Receiver buffer not empty interrupt.
 * @param[in] idle_int Idle line interrupt.
 * @param[in] err_int Error interrupt.
 */
void usart_configure_interrupt(size_t port, bool parity_err_int, bool tx_buf_empty_int, bool tx_complete_int,
			       bool rx_buf_not_empty_int, bool idle_int, bool err_int);

/**
 * @brief Control USART port DMA.
 *
 * @param[in] port USART port.
 * @param[in] dma_tx Transceiver DMA.
 * @param[in] dma_rx Receiver DMA.
 * @param[in] status Port status.
 */
void usart_dma(size_t port, bool dma_tx, bool dma_rx);
