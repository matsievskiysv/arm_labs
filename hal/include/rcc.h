#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/rcc.h"
#elif defined(set32f103h)
#include "../stm32f103h/rcc.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

/**
 * @brief Enable bits in RCC register.
 *
 * @param[in] reg Register offset.
 * @param[in] value Register value.
 */
void rcc_set_register(size_t reg, size_t value);

/**
 * @brief Write to RCC register.
 *
 * @param[in] reg Register offset.
 * @param[in] value Register value.
 */
void rcc_write_register(size_t reg, size_t value);

/**
 * @brief Retrieve RCC register value.
 *
 * @param[in] reg Register offset.
 * @return Register value.
 */
size_t rcc_get_register(size_t reg);

/**
 * @brief Start internal 8MHz clock.
 */
void rcc_start_internal_clock(void);

/**
 * @brief Set ADC clock prescaler.
 *
 * @param[in] val Prescaler value.
 */
void rcc_adc_prescaler(size_t val);
