#pragma once

#include <stddef.h>

/**
 * @brief Internal register type.
 */
#define REG_T		       volatile size_t *const

/**
 * @brief Get value with bit \p X set to 1.
 */
#define BIT(X)		       (1 << X)

/**
 * @brief Get absolute register address from \p BASE address and \p OFFSET.
 */
#define GET_REG(BASE, OFFSET)  ((size_t *) ((BASE) + (OFFSET)))

/**
 * @brief Access register value.
 */
#define REG_VAL(REG)	       (*REG)

/**
 * @brief Write value to \p REG.
 */
#define REG_WRITE(REG, VAL)    *REG = (VAL)

/**
 * @brief Set bits in \p REG from \p MASK to 1.
 */
#define REG_SET(REG, MASK)    *REG |= (MASK)

/**
 * @brief Set bits in \p REG from \p MASK to 0.
 */
#define REG_CLEAR(REG, MASK)  *REG &= ~(MASK)

/**
 * @brief Toggle bits in \p REG from \p MASK.
 */
#define REG_TOGGLE(REG, MASK) *REG ^= (MASK)

/**
 * @brief Wait until \p COND is false.
 */
#define WAIT_WHILE(COND)                                                                                               \
	while (COND) {}

/**
 * @brief Loop indefinitely.
 */
#define LOOP(BODY)                                                                                                     \
	for (;;) {                                                                                                     \
		BODY                                                                                                   \
	}
