#!/usr/bin/env bash

rm -f compile_commands.json
find . -type f -name 'compile_commands.json' -exec jq -n 'inputs' {} + | tee compile_commands.json
