#define _DEFAULT_SOURCE

#include <gpio.h>
#include <nvic.h>
#include <rcc.h>
#include <signature.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <usart.h>
#include <util.h>

#define FREQ	       8000000
#define BAUD	       9600
#define UART_PORT      USART1
#define UART_GPIO_PORT GPIO_A
#define UART_PIN_TX    9
#define UART_PIN_RX    10

int __attribute__((noreturn)) main(void)
{
	// enable clocks
	{
		rcc_apb2enr_t apb2enr = {.bits = {.iopaen = true, .usart1en = true}};
		rcc_set_register(RCC_APB2ENR, apb2enr.val);
	}
	{
		rcc_ahbenr_t ahbenr = {.bits = {.dma1en = true}};
		rcc_set_register(RCC_AHBENR, ahbenr.val);
	}

	// configure pins for UART
	gpio_configure_output(UART_GPIO_PORT, UART_PIN_TX, GPIO_ALT_PUSH, GPIO_SPEED_LOW);
	gpio_configure_input(UART_GPIO_PORT, UART_PIN_RX, GPIO_IN_PULL, true);

	// configure UART
	usart_configure(UART_PORT, false, false, false, false, USART_STOP_1);
	usart_baudrate(UART_PORT, FREQ, BAUD);

	usart_enable(UART_PORT);

	for (;;) {
		// wait for UART message
		WAIT_WHILE(!usart_status(UART_PORT).bits.rxne);
		uint8_t chr = usart_read(UART_PORT);

		/***************************/
		/* insert code here	   */
		/***************************/

		usart_write(UART_PORT, chr);
		// wait for UART transmission to end
		WAIT_WHILE(!usart_status(UART_PORT).bits.txe);
	}
}
