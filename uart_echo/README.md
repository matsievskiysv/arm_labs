## Compile project

Install dependencies

```bash
sudo apt install build-essential make gcc-arm-none-eabi openocd git gdb-multiarch
```

Clone project

```bash
git clone --recursive https://gitlab.com/matsievskiysv/arm_labs.git && cd arm_labs/uart_echo
```

Build project

```bash
make
```

Clear project

```bash
make clean
```

Start debugger

```bash
make debug
```

## Debug commands

* `next`: step to the next line of code
* `step`: step into the function
* `continue`: continue running command until breakpoint or <kbd>Ctrl+c</kbd> key press
* `print <var>`: print contents of variable `<var>`

## Serial configuration

Configure serial port communication

```bash
stty -F /dev/ttyUSB0 9600 cs8 -cstopb -parenb -parodd -echo extproc
```

Read messages

```bash
cat < /dev/ttyUSB0
```

Send messages

```bash
echo -en 'hello world\r\n' > /dev/ttyUSB0
```
